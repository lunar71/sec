/*

Append the following to ~/.arduino15/packages/digistump/hardware/avr/<version>/libraries/DigisparkMouse/usbconfig.h
Remove after done
Device VID/PID: https://www.the-sz.com/products/usbid/index.php?v=0x045E&p=0x0053

#ifdef USB_CFG_VENDOR_ID
#undef USB_CFG_VENDOR_ID
#define USB_CFG_VENDOR_ID 0x53, 0x00
#endif

#ifdef USB_CFG_DEVICE_ID
#undef USB_CFG_DEVICE_ID
#define USB_CFG_DEVICE_ID 0x5e, 0x04
#endif

#ifdef USB_CFG_VENDOR_NAME
#undef USB_CFG_VENDOR_NAME
#define USB_CFG_VENDOR_NAME     'M','i','c','r','o','s','o','f','t',' ','C','o','r','p','o','r','a','t','i','o','n'
#endif

#ifdef USB_CFG_VENDOR_NAME_LEN
#undef USB_CFG_VENDOR_NAME_LEN
#define USB_CFG_VENDOR_NAME_LEN 21
#endif

#ifdef USB_CFG_DEVICE_NAME
#undef USB_CFG_DEVICE_NAME
#define USB_CFG_DEVICE_NAME     'O','p','t','i','c','a','l',' ','M','o','u','s','e'
#endif

#ifdef USB_CFG_DEVICE_NAME_LEN
#undef USB_CFG_DEVICE_NAME_LEN
#define USB_CFG_DEVICE_NAME_LEN 13
#endif

#ifdef USB_CFG_SERIAL_NUMBER
#undef USB_CFG_SERIAL_NUMBER
#define USB_CFG_SERIAL_NUMBER   'M','i','c','r','o','s','o','f','t',' ','C','o','r','p','o','r','a','t','i','o','n'
#endif

#ifdef USB_CFG_SERIAL_NUMBER_LEN
#undef USB_CFG_SERIAL_NUMBER_LEN
#define USB_CFG_SERIAL_NUMBER_LEN   21
#endif

*/

#include <DigiMouse.h>

unsigned int cycle_low = 500; 
unsigned int cycle_high = 500;

void setup() {
  randomSeed(analogRead(0));
  pinMode(1, OUTPUT);
  DigiMouse.begin();
}

void loop() {
  digitalWrite(1, HIGH);
  DigiMouse.delay(50);
  digitalWrite(1, LOW);
  
  DigiMouse.moveY(-3);
  DigiMouse.delay(50);
  DigiMouse.delay(random(cycle_low, cycle_high));
  
  DigiMouse.moveX(3);
  DigiMouse.delay(50);
  DigiMouse.delay(random(cycle_low, cycle_high));

  DigiMouse.moveY(3);
  DigiMouse.delay(50);
  DigiMouse.delay(random(cycle_low, cycle_high));

  DigiMouse.moveX(-3);
  DigiMouse.delay(50);
  DigiMouse.delay(random(cycle_low, cycle_high));
}
