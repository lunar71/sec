#!/bin/bash

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
RESET="\033[0m"
GOOD_MSG="${GREEN}[+]${RESET} "
BAD_MSG="${RED}[-]${RESET} "
Q_MSG="[0;33m[?][0m"

LUKS_NAME="crypt"
LUKS_MAPPER_PATH="/dev/mapper/${LUKS_NAME}"


function ask_dev {
    read -p "Enter device (e.g. /dev/sdX, *without a number*): " -r DEVICE
    PART="${DEVICE}1"
    if [[ ! -b ${DEVICE} ]]; then
        echo -e "${BAD_MSG}Device \"${DEVICE}\" doesn't appear to exist under /dev.\n${BAD_MSG}Make sure you entered a correct device (e.g. /dev/sdX, *without a number*)."
        exit 1
    else
        echo -e "${GOOD_MSG}Device \"${DEVICE}\" confirmed to exist. Continuing..."
    fi
}


function create_luks {
    echo
cat << EOF
sudo parted -s ${DEVICE} mklabel msdos mkpart primary 0% 100% && \\
sudo cryptsetup luksFormat -c aes-xts-plain64 -s 512 -h sha512 -i 5000 ${PART} && \\
sudo cryptsetup luksOpen ${PART} ${LUKS_NAME} && \\
sudo mkfs.ext4 -L ${LUKS_NAME} ${LUKS_MAPPER_PATH} && \\
sudo cryptsetup luksClose ${LUKS_NAME}

EOF
}


function ask_confirmation {
    echo
    ask_dev
    read -p "${Q_MSG} Is device \"${DEVICE}\" correct? [Yy/Nn]: " -r REPLY1
    if [[ $REPLY1 =~ ^[Yy]$ ]]; then
        read -p "${Q_MSG} Selected device is \"${DEVICE}\". ARE YOU SURE? [Yy/Nn]: " -r REPLY2
        if [[ $REPLY2 =~ ^[Yy]$ ]]; then
            echo -e "${GOOD_MSG}Generated commands follow; please review them, then issue them:"
            create_luks
        else
            echo -e "${BAD_MSG}Incorrect/unrecognized confirmation [Yy/Nn]. Exiting..."
            exit 1
        fi
    else
        echo -e "${BAD_MSG}Incorrect/unrecognized confirmation [Yy/Nn]. Exiting..."
        exit 1
    fi
}

ask_confirmation
