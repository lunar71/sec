#!/bin/bash

ENTRYPOINT=$(mktemp -p .)
cat > $ENTRYPOINT << EOF
#!/bin/sh
mkdir -p /var/wsgidav-root /etc/wsgidav/certs
case \$WEBDAV_ENV in
    "http")
        wsgidav --host 0.0.0.0 --port 80 --root /var/wsgidav-root --auth=anonymous;;
    "https")
        openssl req -x509 -newkey rsa:2048 -days 3650 -keyout /etc/wsgidav/certs/server.key -out /etc/wsgidav/certs/server.crt -nodes -subj "/C=US/ST=State/L=Cityopia/O=Company/CN=127.0.0.1"
        printf "ssl_certificate: /etc/wsgidav/certs/server.crt\nssl_private_key: /etc/wsgidav/certs/server.key" >/etc/wsgidav/wsgidav.yaml
        wsgidav --host 0.0.0.0 --port 443 --root /var/wsgidav-root --auth=anonymous --config=/etc/wsgidav/wsgidav.yaml;;
esac
EOF

docker build -t wsgidav-docker -f - . << EOF
    FROM python:3-alpine
    RUN apk add --no-cache --virtual .build-deps gcc libxslt-dev musl-dev py3-lxml py3-pip \
        && pip3 install --no-cache-dir wsgidav cheroot lxml \
        && apk del .build-deps gcc musl-dev \
        && apk add --no-cache openssl
    EXPOSE 80
    EXPOSE 443
    COPY "$ENTRYPOINT" /entrypoint.sh
    RUN chmod +x /entrypoint.sh
    ENTRYPOINT ["/bin/sh", "/entrypoint.sh"]
EOF

rm $ENTRYPOINT

cat > "${HOME}/.local/bin/webdav" << EOF
#!/bin/bash

[[ -z \$1 ]] && printf "Usage: \$0 <http/https/logs/stop>\n" && exit 1

case \$1 in
    "http")
        docker run -d -e "WEBDAV_ENV=http" --rm --hostname="wsgidav-docker" --name="wsgidav-docker" -p80:80 --volume=\$(pwd):/var/wsgidav-root wsgidav-docker
        ;;
    "https")
        docker run -d -e "WEBDAV_ENV=https" --rm --hostname="wsgidav-docker" --name="wsgidav-docker" -p443:443 --volume=\$(pwd):/var/wsgidav-root wsgidav-docker
        ;;
    "logs")
        docker logs -f wsgidav-docker
        ;;
    "stop")
        docker stop wsgidav-docker
        ;;
esac
EOF
chmod +x "${HOME}/.local/bin/webdav"
