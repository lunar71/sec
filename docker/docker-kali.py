#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import os
import ast
import json
import stat
import argparse
import subprocess
from textwrap import dedent

class DockerfileBuilder(object):
    def __init__(self, build_type, tools_json):
        self.image = 'kalilinux/kali-rolling'
        self.volume = '/root/shared'
        self.workdir = '/root'
        self.tools_dir = '/opt/tools'
        self.py3_virtualenv = f'{self.workdir}/py3env'
        self.tools_json = tools_json
        self.build_type = build_type
        self.pre_run_cmd = 'apt update -y && apt upgrade -y && apt upgrade -y'
        self.post_run_cmd = f'gem install winrm winrm-fs; tree --dirsfirst -n -L 2 {self.tools_dir} > {self.workdir}/tools.txt'
        self.run_cmd = 'apt install -y'

        # careful escaping these alongside + printf '%s\n'
        self.bashrc = [
                "\"PS1='\[\e[0;91m\]\h\[\e[0m\] in \[\e[0;38;5;33m\]\$PWD \[\e[0m\]\$ \[\e[0m\]'\"",
                repr("alias ffuf='ffuf -p \'0.8-1.5\''"),
                repr("IGNOREEOF=10")
        ]

    def _build_type_chooser(self):
        btypes = {
            'core': 'kali-linux-core',
            'default': 'kali-linux-default',
            'custom': 'kali-linux-core vim arping binwalk bettercap \
                cadaver cewl chisel crackmapexec ffuf dirsearch enum4linux \
                exploitdb fping gobuster hashcat hashcat-utils hydra impacket-scripts \
                maskprocessor masscan metasploit-framework nbtscan netdiscover \
                nikto nmap patator powersploit python3-pip proxychains proxytunnel \
                radare2 responder smbmap snmpcheck sqlmap sslscan unix-privesc-check \
                virtualenv voiphopper wafw00f webshells weevely whatweb wordlists \
                wpscan git socat sqlitebrowser musl tree p0f libpcap-dev dnsmasq neo4j ncat'
        }
        return btypes

    def _extras(self):
        if self.build_type != 'core':
            return r"""
            COPY --from=golang:1.17-alpine /usr/local/go/ /usr/local/go/
            ENV GOROOT="/usr/local/go"
            ENV GOPATH="/root/go"
            ENV PATH="${PATH}:${GOROOT}/bin"
            ENV PATH="${PATH}:${GOPATH}/bin"

            RUN GO111MODULE=on go get -v github.com/projectdiscovery/httpx/cmd/httpx
            RUN GO111MODULE=on go get -v github.com/projectdiscovery/subfinder/v2/cmd/subfinder
            RUN GO111MODULE=on go get -v github.com/projectdiscovery/nuclei/v2/cmd/nuclei
            RUN GO111MODULE=on go get -v github.com/lc/gau
            RUN GO111MODULE=on go get -v github.com/OWASP/Amass/v3/...
            RUN GO111MODULE=auto go get -v github.com/michenriksen/aquatone
            """
    def _parse_tools(self):
        if self.build_type == 'custom':
            try:
                with open(self.tools_json) as fh:
                    tools = json.loads(
                                json.dumps(
                                    ast.literal_eval(
                                        fh.read()
                                    )
                                )
                            )

                tools_dict = dict()
                for k,v in tools.items():
                    tools_dict[os.path.join(self.tools_dir, k)] = v

                return tools_dict

            except FileNotFoundError as e:
                pass

    def _build_dockerfile(self):
        btype = re.sub(' +', ' ', self._build_type_chooser()[self.build_type])
        extras = self._extras()
        tools_dict = self._parse_tools()
        dockerfile = f"""
            FROM {self.image}
            ENV DEBIAN_FRONTEND noninteractive
            RUN {self.pre_run_cmd}
            RUN {self.run_cmd} {btype}
            VOLUME {self.volume}
            WORKDIR {self.workdir}
        """

        if extras:
            dockerfile += f"""
                {extras}
            """

        if tools_dict:
            for directory, tools in tools_dict.items():
                t = ' '.join(tools)
                dockerfile += f"""
                    ARG {os.path.basename(directory)}="{t}"
                    RUN ["/bin/bash", "-c", "mkdir -p {directory}"]
                """

                if os.path.basename(directory) == 'bin':
                    dockerfile += f"""
                        RUN ["/bin/bash", "-c", "for i in $bin; do wget -q --show-progress --no-check-certificate -nc $i -P {directory}; done"]
                    """
                else:
                    dockerfile += f"""
                        RUN ["/bin/bash", "-c", "for i in ${os.path.basename(directory)}; do git -C {directory} clone --progress $i; done"]
                    """

        dockerfile += f"""
            RUN {self.post_run_cmd}
        """

        dockerfile += f"""
            RUN ["virtualenv", "-p", "python3", "{self.py3_virtualenv}"]
            RUN ["/bin/bash", "-c", "source {self.py3_virtualenv}/bin/activate && pip install Cython && pip install python-libpcap && for i in $(find /opt/tools -name setup.py); do cd $(dirname $i) && python3 setup.py install; done"]
            RUN ["/bin/bash", "-c", "rm -rf {self.workdir}/{{.local,.cache,.wget-hsts}}"]
            RUN ["/bin/bash", "-c", "apt clean -y && apt autoclean -y && apt autoremove -y"]
        """

        for i in self.bashrc:
            dockerfile += f"""
                RUN printf '%s\\n' {i} >> {self.workdir}/.bashrc
            """
        return {self.build_type: dockerfile}

class ImageBuilder(object):
    def __init__(self, dockerfile):
        self.command = dedent(f"""
            docker build -t docker-kali-{''.join(dockerfile.keys())} - << "EOF"
                {''.join(dockerfile.values())}
            EOF
        """)
        self.launcher_path = os.path.join(os.environ['HOME'], '.local', 'bin', 'kali')
        self.launcher = dedent(f"""
            #!/bin/bash

            xhost +local:$(hostname) >/dev/null 2>&1
            RAND=$(printf $(tr -dc 'a-z0-9' </dev/urandom | head -c 10))

            case $1 in
                commit)
                    docker run -it --name="docker-kali-commit" --hostname="docker-kali-commit" --net="host" docker-kali-{''.join(dockerfile.keys())} /bin/bash

                    CID=$(docker container ls -a | grep -i docker-kali-commit | awk '{{print $1}}')
                    docker commit $CID docker-kali
                    docker container rm $CID --force
                    ;;
                export)
                    docker save docker-kali-custom | xz -v6 > docker-kali-custom.tar.xz
                    ;;

                vpn)
                    OVPN_CONFIG_PATH=$2
                    OVPN_CONFIG_NAME="$(echo $(basename $OVPN_CONFIG_PATH) | sed 's/@/-at-/')"
                    OVPN_CONFIG_FILE="$(basename $OVPN_CONFIG_PATH)"
                    OVPN_CONFIG_DIR="$(dirname $OVPN_CONFIG_PATH)"

                    if [[ -f $OVPN_CONFIG_PATH ]]; then
                        docker run --rm -it -d --cap-add=NET_ADMIN -v=$OVPN_CONFIG_DIR:/ovpn_configs:ro --dns=1.1.1.1 --dns=8.8.4.4 --dns=8.8.8.8 --device=/dev/net/tun --name=ovpn-client_$OVPN_CONFIG_NAME ovpn-client $OVPN_CONFIG_FILE >/dev/null

                    until [[ 'tun0' =~ $(docker exec -it "ovpn-client_${{OVPN_CONFIG_NAME}}" ip route get 8.8.8.8 | grep -Po '(?<=(dev )).*(?= src| proto)') ]]; do
                        echo "Waiting for VPN connection..."
                        sleep 1
                    done

                        # bug with --hostname="docker-kali_$OVPN_CONFIG_NAME" and --net=container
                        docker run --rm -it --name="docker-kali_$OVPN_CONFIG_NAME" --net=container:"ovpn-client_${{OVPN_CONFIG_NAME}}" -e DISPLAY=$DISPLAY -v=/tmp/.X11-unix:/tmp/.X11-unix -v=$HOME/.Xauthority:/root/.Xauthority:rw -v="$(pwd):/root/shared" docker-kali-custom /bin/bash && docker stop "ovpn-client_${{OVPN_CONFIG_NAME}}"
                    fi
                    ;;

                    *)
                        docker run --rm -it --name="docker-kali_$RAND" --hostname="docker-kali" --net="host" --privileged -e DISPLAY=$DISPLAY -v=/tmp/.X11-unix:/tmp/.X11-unix -v=$HOME/.Xauthority:/root/.Xauthority:rw -v="$(pwd):/root/shared" docker-kali-{''.join(dockerfile.keys())} /bin/bash
                        ;;
            esac

            xhost -local:$(hostname) >/dev/null 2>&1
        """).strip()

    def build(self):
        os.system(self.command)
        ovpn_client = """
            FROM alpine:latest
            RUN apk add --no-cache curl openvpn
            RUN mkdir /ovpn_configs
            WORKDIR /ovpn_configs
            ENTRYPOINT ["openvpn", "--config"]
        """
        os.system("""
        docker build -t ovpn-client - << EOF
            {ovpn_client}
EOF""".format(ovpn_client=ovpn_client))

    def create_launcher(self):
        with open(self.launcher_path, 'w') as fh:
            fh.write(self.launcher)
            os.chmod(self.launcher_path, 0o744)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-b', '--build-type', required=True, choices=['core', 'default', 'custom'])
    parser.add_argument('-t', '--tools-json', default=os.path.join(os.curdir, 'tools.json'))

    args = parser.parse_args()

    DockerfileBuilder = DockerfileBuilder(args.build_type, args.tools_json)
    dockerfile = DockerfileBuilder._build_dockerfile()

    builder = ImageBuilder(dockerfile)
    builder.build()
    builder.create_launcher()

