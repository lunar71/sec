#!/bin/bash

FAIL="\033[31m[-]\033[00m"
GOOD="\033[32m[+]\033[00m"
WARNING="\033[33m[!]\033[00m"
INFO="\033[34m[*]\033[00m"

cat << EOF | docker build -t android-studio-ppa -
FROM ubuntu:20.04

RUN apt update -y
RUN apt install -y software-properties-common adb
RUN add-apt-repository ppa:maarten-fonville/android-studio
RUN apt update -y
RUN apt install -y android-studio

ENV DEBIAN_FRONTEND=noninteractive
RUN apt install -y xorg vim ant maven

RUN apt clean
EOF

printf "\n"
printf "${GOOD} Successfully built docker container with Android Studio from ppa:maarten-fonville/android-studio\n"
printf "${WARNING}${WARNING}${WARNING} WARNING: To run docker Android Studio GUI, X11 access control list will be modified prior to running this image and will be revereted upon exiting\n"
printf "${INFO} NOTE: X11 access control modifiers: \"xhost +local:\$HOSTNAME\" and \"xhost -local:\$HOSTNAME\"\n"
printf "${INFO} Start Android Studio with /opt/android-studio/bin/studio.sh\n"
printf "${INFO} Will map \$(pwd) to /root/AndroidStudioProjects to access projects and builds\n"
printf "${INFO} The following Bash function will be appended to your $HOME/.bashrc\n"

cat > "${HOME}/.local/bin/dandroidstudio" << "EOF"
#!/bin/bash

AS_DIR="${HOME}/.docker-android-studio"
[[ ! -d "${AS_DIR}" ]] && mkdir -p "${AS_DIR}"
#cd $AS_DIR

xhost +local:$(hostname)

docker run --rm -it -h="android-studio-docker" -e="DISPLAY" --device /dev/kvm -v=/tmp/.X11-unix:/tmp/.X11-unix -v=${HOME}/.Xauthority:/root/.Xauthority:rw -v=${AS_DIR}/AndroidStudioProjects:/root/AndroidStudioProjects android-studio-ppa /bin/bash -c /opt/*/bin/studio.sh

xhost -local:$(hostname)
EOF
chmod +x "${HOME}/.local/bin/dandroidstudio"
