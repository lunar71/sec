#!/bin/bash
# -*- coding: utf-8 -*-

[[ -z $1 ]] && printf "Usage: $0 <default|custom>\n" && exit 1

[[ ! $(command -v docker) ]] && printf "Docker not found, exiting...\n" && exit 2
[[ ! $(command -v jq) ]] && printf "jq not found, exiting...\n" && exit 2

case $1 in
    default)
        packages="kali-linux-default"
        gems=""
        ;;
    custom)
        packages="kali-linux-core vim arping binwalk bettercap \
            cadaver cewl chisel crackmapexec ffuf dirsearch enum4linux \
            exploitdb fping gobuster hashcat hashcat-utils hydra impacket-scripts \
            maskprocessor masscan metasploit-framework nbtscan netdiscover \
            nikto nmap patator powersploit python3-pip proxychains proxytunnel \
            radare2 responder smbmap snmpcheck sqlmap sslscan unix-privesc-check \
            virtualenv voiphopper wafw00f webshells weevely whatweb wordlists \
            wpscan git socat sqlitebrowser musl tree p0f libpcap-dev dnsmasq neo4j ncat \
            python3-dev build-essential libnl-3-dev libnl-genl-3-dev libssl-dev krb5-* \
            passing-the-hash libkrb5-dev"
        gems="winrm winrm-fs"
        ;;
esac

declare -A tools
tools["wordlists"]="https://github.com/danielmiessler/SecLists https://github.com/FlameOfIgnis/Pwdb-Public https://github.com/swisskyrepo/PayloadsAllTheThings https://github.com/fuzzdb-project/fuzzdb https://github.com/1ndianl33t/Gf-Patterns https://github.com/dwisiswant0/gf-secrets https://github.com/danielmiessler/RobotsDisallowed https://github.com/cujanovic/subdomain-bruteforce-list https://github.com/cujanovic/Open-Redirect-Payloads https://github.com/cujanovic/Markdown-XSS-Payloads https://github.com/cujanovic/CRLF-Injection-Payloads https://github.com/cujanovic/Virtual-host-wordlist https://github.com/cujanovic/resolvers https://github.com/cujanovic/Linux-default-files-images-location https://github.com/arnaudsoullie/ics-default-passwords https://github.com/jeanphorn/wordlist https://github.com/nyxxxie/awesome-default-passwords https://github.com/1N3/IntruderPayloads https://github.com/berzerk0/Probable-Wordlists https://github.com/BBhacKing/jwt_secrets https://github.com/wallarm/jwt-secrets https://github.com/ihebski/DefaultCreds-cheat-sheet https://gist.github.com/8e12adefbd505ef704674ad6ad48743d.git https://github.com/random-robbie/bruteforce-lists https://github.com/sharsi1/russkiwlst"

tools["web"]="https://github.com/tomnomnom/waybackurls https://github.com/tomnomnom/httprobe https://github.com/tomnomnom/assetfinder https://github.com/lc/subjs https://github.com/lc/gau https://github.com/projectdiscovery/cloudlist https://github.com/projectdiscovery/naabu https://github.com/projectdiscovery/subfinder https://github.com/projectdiscovery/httpx https://github.com/projectdiscovery/nuclei https://github.com/shmilylty/OneForAll https://github.com/OJ/gobuster https://github.com/michenriksen/aquatone https://github.com/ffuf/ffuf https://github.com/OWASP/Amass https://github.com/s0md3v/Arjun https://github.com/GerbenJavado/LinkFinder https://github.com/m4ll0k/SecretFinder https://github.com/brendan-rius/c-jwt-cracker https://github.com/OWASP/wstg https://github.com/maurosoria/dirsearch https://github.com/sa7mon/S3Scanner https://github.com/Dionach/CMSmap https://github.com/urbanadventurer/WhatWeb https://github.com/wpscanteam/wpscan https://github.com/zseano/InputScanner https://github.com/zseano/JS-Scan https://github.com/maK-/parameth https://github.com/drego85/JoomlaScan https://github.com/hakluke/hakrawler https://github.com/mbechler/marshalsec https://github.com/rbsec/sslscan https://github.com/IAmStoxe/urlgrab https://github.com/wireghoul/htshells https://github.com/m4ll0k/Bug-Bounty-Toolz https://github.com/chris408/ct-exposer https://github.com/mazen160/server-status_PWN https://github.com/s0md3v/Parth https://github.com/irsdl/IIS-ShortName-Scanner https://github.com/swisskyrepo/GraphQLmap"
tools["mobile"]="https://github.com/AloneMonkey/frida-ios-dump"

tools["windows"]="https://github.com/vletoux/pingcastle https://github.com/dirkjanm/PrivExchange https://github.com/dafthack/MailSniper https://github.com/malcomvetter/CSExec https://github.com/mdsecactivebreach/Farmer https://github.com/rasta-mouse/MiscTools https://github.com/two06/Inception https://github.com/fullmetalcache/RunSharp https://github.com/antonioCoco/RunasCs https://github.com/3xpl01tc0d3r/Minidump https://github.com/m0rv4i/SafetyDump https://github.com/antonioCoco/SharPyShell https://github.com/checkymander/Sharp-WMIExec https://github.com/checkymander/Sharp-SMBExec https://github.com/b4rtik/SharpAdidnsdump https://github.com/Flangvik/SharpAppLocker https://github.com/CCob/SharpBlock https://github.com/rvrsh3ll/SharpCOM https://github.com/FSecureLABS/SharpClipHistory https://github.com/chrismaddalena/SharpCloud https://github.com/anthemtotheego/SharpCradle https://github.com/GhostPack/SharpDPAPI https://github.com/HunnicCyber/SharpDomainSpray https://github.com/infosecn1nja/SharpDoor https://github.com/rvrsh3ll/SharpEdge https://github.com/anthemtotheego/SharpExec https://github.com/fullmetalcache/SharpFiles https://github.com/s0lst1c3/SharpFinder https://github.com/rvrsh3ll/SharpFruit https://github.com/FSecureLABS/SharpGPOAbuse https://github.com/outflanknl/SharpHide https://github.com/IlanKalendarov/SharpHook https://github.com/BloodHoundAD/SharpHound3 https://github.com/b4rtik/SharpLoadImage https://github.com/djhohnstein/WireTap https://github.com/shantanu561993/SharpLoginPrompt https://github.com/cube0x0/SharpMapExec https://github.com/0xthirteen/SharpMove https://github.com/mdsecactivebreach/SharpPack https://github.com/rvrsh3ll/SharpPrinter https://github.com/djhohnstein/SharpSC https://github.com/rvrsh3ll/SharpSSDP https://github.com/djhohnstein/SharpShares https://github.com/HunnicCyber/SharpSniper https://github.com/nettitude/SharpSocks https://github.com/jnqpblc/SharpTask https://github.com/GhostPack/SharpWMI https://github.com/djhohnstein/SharpWeb https://github.com/malcomvetter/SneakyService https://github.com/Mr-B0b/SpaceRunner https://github.com/mgeeky/Stracciatella https://github.com/Arno0x/TCPRelayInjecter2 https://github.com/jfmaes/CMDLL https://github.com/bitsadmin/nopowershell https://github.com/thecybermafia/OffensivePowerShell https://github.com/mattifestation/PowerShellArsenal https://github.com/p3nt4/PowerShdll https://github.com/giMini/PowerMemory https://github.com/nickrod518/Create-EXEFromPS1 https://github.com/leoloobeek/LAPSToolkit https://github.com/hfiref0x/UACME https://github.com/EncodeGroup/UAC-SilentClean https://github.com/padovah4ck/PSByPassCLM https://github.com/TheWover/donut https://github.com/fireeye/SharPersist https://github.com/Flangvik/SharpCollection https://github.com/Arvanaghi/SessionGopher https://github.com/giuliano108/SeBackupPrivilege https://github.com/NetSPI/PowerUpSQL https://github.com/PowerShellMafia/PowerSploit https://github.com/nidem/kerberoast https://github.com/Hackplayers/evil-winrm https://github.com/besimorhino/powercat https://github.com/p3nt4/Invoke-SocksProxy https://github.com/cyberark/RiskySPN https://github.com/GhostPack/Rubeus https://github.com/Kevin-Robertson/Inveigh https://github.com/Kevin-Robertson/Invoke-TheHash https://github.com/HarmJ0y/KeeThief https://github.com/HarmJ0y/SharpClipboard https://github.com/0x09AL/RdpThief https://github.com/rasta-mouse/AmsiScanBufferBypass https://github.com/outflanknl/Recon-AD https://github.com/0xthirteen/SharpRDP https://github.com/cobbr/SharpSploit https://github.com/NullArray/MaliciousDLLGen https://github.com/matterpreter/DefenderCheck https://github.com/Binject/go-donut https://github.com/samratashok/nishang https://github.com/GhostPack/Lockless https://github.com/GhostPack/SharpUp https://github.com/GhostPack/SafetyKatz https://github.com/GhostPack/SharpDump https://github.com/GhostPack/Seatbelt https://github.com/G0ldenGunSec/SharpSecDump https://github.com/Apr4h/CobaltStrikeScan https://github.com/mindcrypt/powerglot https://github.com/jfmaes/SharpXOR https://github.com/mez-0/DecryptRDCManager https://github.com/b4rtik/SharpKatz https://github.com/tokyoneon/chimera https://github.com/b4rtik/HiddenPowerShellDll https://github.com/Mr-Un1k0d3r/DLLsForHackers https://github.com/nccgroup/nccfsas https://github.com/trustedsec/unicorn https://github.com/411Hall/JAWS https://github.com/M4ximuss/Powerless https://github.com/rasta-mouse/Sherlock https://github.com/S3cur3Th1sSh1t/Amsi-Bypass-Powershell https://github.com/S3cur3Th1sSh1t/WinPwn https://github.com/CompassSecurity/BloodHoundQueries https://github.com/bohops/GhostBuild https://github.com/FortyNorthSecurity/hot-manchego https://github.com/mdsecactivebreach/SharpShooter https://github.com/passthehashbrowns/SharpBuster https://github.com/Greenwolf/ntlm_theft https://github.com/gtworek/PSBits https://github.com/OmerYa/Invisi-Shell https://github.com/mez-0/CSharpWinRM https://github.com/Gerenios/AADInternals https://github.com/samratashok/ADModule https://github.com/l0ss/Grouper2 https://github.com/ropnop/kerbrute https://github.com/eladshamir/Internal-Monologue https://github.com/Kevin-Robertson/Powermad https://github.com/leechristensen/SpoolSample https://github.com/HarmJ0y/DAMP https://github.com/jschicht/RunAsTI https://github.com/Raikia/UhOh365 https://github.com/nullbind/Powershellery https://github.com/PyroTek3/PowerShell-AD-Recon https://github.com/massgravel/Microsoft-Activation-Scripts https://github.com/Hackplayers/Salsa-tools https://github.com/dtrizna/DotNetInject https://github.com/rasta-mouse/TikiTorch https://github.com/yck1509/ConfuserEx https://github.com/peewpw/Invoke-PSImage https://github.com/Pickfordmatt/SharpLocker https://github.com/fullmetalcache/PowerLine https://github.com/Kevin-Robertson/InveighZero https://github.com/antonioCoco/RoguePotato https://github.com/ohpe/juicy-potato https://github.com/breenmachine/RottenPotatoNG"

tools["linux"]="https://github.com/Bashfuscator/Bashfuscator https://github.com/rebootuser/LinEnum https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite https://github.com/Anon-Exploiter/SUID3NUM https://github.com/sleventyeleven/linuxprivchecker https://github.com/diego-treitos/linux-smart-enumeration https://github.com/DominicBreuker/pspy https://github.com/jfredrickson/setuid-wrapper https://github.com/pentestmonkey/unix-privesc-check https://github.com/s0md3v/Decodify https://github.com/skelsec/pypykatz https://github.com/WangYihang/GitHacker https://github.com/BishopFox/GitGot https://github.com/TarlogicSecurity/tickey https://github.com/Zer1t0/ticket_converter https://github.com/TarlogicSecurity/kerbrute https://github.com/blendin/3snake"

tools["network"]="https://github.com/ztgrace/changeme https://github.com/dirkjanm/adidnsdump https://github.com/ginuerzh/gost https://github.com/swisskyrepo/SSRFmap https://github.com/lgandx/PCredz https://github.com/teknogeek/ssrf-sheriff https://github.com/tarunkant/Gopherus https://github.com/kost/revsocks https://github.com/jpillora/chisel https://github.com/bettercap/bettercap https://github.com/klsecservices/rpivot https://github.com/byt3bl33d3r/CrackMapExec https://github.com/robertdavidgraham/masscan https://github.com/BC-SECURITY/Empire https://github.com/BloodHoundAD/BloodHound https://github.com/fox-it/BloodHound.py https://github.com/threat9/routersploit https://github.com/ollypwn/SMBGhost https://github.com/lgandx/Responder https://github.com/nccgroup/vlan-hopping---frogger https://github.com/fox-it/mitm6 https://github.com/ShawnDEvans/smbmap https://github.com/DanMcInerney/net-creds https://github.com/cytopia/pwncat https://github.com/rvrsh3ll/eavesarp https://github.com/lesnuages/hershell https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite https://github.com/RUB-NDS/PRET https://github.com/SecureAuthCorp/impacket"

tools["misc"]="https://github.com/paranoidninja/CarbonCopy https://github.com/projectdiscovery/notify https://github.com/andrew-d/static-binaries https://github.com/ZephrFish/static-tools https://github.com/static-linux/static-binaries-i386 https://github.com/therealsaumil/static-arm-bins https://github.com/projectdiscovery/nuclei-templates https://github.com/vulnersCom/nmap-vulners https://github.com/pr4jwal/CVE-2020-0796 https://github.com/HarmJ0y/CheatSheets https://github.com/ZerBea/hcxtools https://github.com/ZerBea/hcxdumptool https://github.com/hashcat/hashcat-utils https://github.com/sdcampbell/Internal-Pentest-Playbook https://github.com/kmkz/Pentesting https://github.com/megadose/holehe https://github.com/nil0x42/duplicut https://github.com/Wenzel/checksec.py https://github.com/jakejarvis/awesome-shodan-queries https://github.com/shifa123/shodandorks https://github.com/codebox/homoglyph https://github.com/tokyoneon/Arcane https://github.com/blackberry/pe_tree https://github.com/Ciphey/Ciphey https://github.com/vysecurity/LinkedInt https://github.com/awsmhacks/awsmBloodhoundCustomQueries https://github.com/S1ckB0y1337/Active-Directory-Exploitation-Cheat-Sheet https://github.com/leostat/rtfm https://github.com/dockcross/dockcross https://github.com/internetwache/GitTools"

#tools["bin"]="https://jitpack.io/com/github/frohoff/ysoserial/master-SNAPSHOT/ysoserial-master-SNAPSHOT.jar https://github.com/BloodHoundAD/BloodHound/releases/download/4.0.1/BloodHound-linux-x64.zip https://github.com/pwntester/ysoserial.net/releases/download/v1.34/ysoserial-1.34.zip  https://github.com/hashcat/hashcat/releases/download/v6.1.1/hashcat-6.1.1.7z https://github.com/gentilkiwi/mimikatz/releases/download/2.2.0-20200918-fix/mimikatz_trunk.7z https://raw.githubusercontent.com/iBotPeaches/Apktool/master/scripts/linux/apktool https://bitbucket.org/iBotPeaches/apktool/downloads/apktool_2.4.1.jar https://github.com/pxb1988/dex2jar/releases/download/2.0/dex-tools-2.0.zip https://github.com/appium-boneyard/sign/releases/download/1.0/sign-1.0.jar https://github.com/appium-boneyard/sign/releases/download/1.0/signapk-1.0.jar https://github.com/java-decompiler/jd-gui/releases/download/v1.6.6/jd-gui-1.6.6-min.jar https://download.sysinternals.com/files/SysinternalsSuite.zip"

BASHRC=$(cat << 'EOF'
HISTCONTROL=ignoreboth
VIRTUAL_ENV_DISABLE_PROMPT=1
IGNOREEOF=10
PROMPT_ALTERNATIVE=twoline
NEWLINE_BEFORE_PROMPT=yes
HISTSIZE=1000
HISTFILESIZE=2000
shopt -s checkwinsize
shopt -s histappend

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias ll='ls --color=auto -lah'
    alias ls='ls --color=auto -lah'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias diff='diff --color=auto'
    alias ip='ip --color=auto'

    export LESS_TERMCAP_mb=$'\E[1;31m'     # begin blink
    export LESS_TERMCAP_md=$'\E[1;36m'     # begin bold
    export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
    export LESS_TERMCAP_so=$'\E[01;33m'    # begin reverse video
    export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
    export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
    export LESS_TERMCAP_ue=$'\E[0m'        # reset underline
fi

alias ..='cd ..'
alias ...='cd ../..'
alias cd..='cd ..'
alias cd...='cd ../..'
alias d='cd ~/Downloads'
alias mkdir='mkdir -pv'
alias cp='cp -r'
alias cat='cat -v'
alias vim='vim -p'

reload() { source ~/.bashrc; }
tmp() { cd $(mktemp -d); }
myip() { arr=(https://ifconfig.me https://checkip.amazonaws.com https://ifconfig.co); for i in "${arr[@]}"; do echo $(curl -sSkL4 $i); done; }
_venv() { [[ $VIRTUAL_ENV ]] && printf "($(basename $VIRTUAL_ENV 2>/dev/null)) "; }

function venv() {
    [[ -z $1 ]] && printf "Create python2 or python3 virtualenv and source it\nUsage: ${FUNCNAME[0]} 2|3\n" && return
    [[ ! $(command -v virtualenv) ]] && printf "virtualenv package not installed\n" && return
    RAND=$(printf $(tr -dc 'a-z0-9' </dev/urandom | head -c 5))
    case $1 in
        "2")
            virtualenv -p python2 "py2venv-$RAND" && source "py2venv-$RAND/bin/activate";;
        "3")
            virtualenv -p python3 "py3venv-$RAND" && source "py3venv-$RAND/bin/activate";;
        *) printf "Unknown option\n";;
    esac
}

export PS1="\[\e[90m\]\$(_venv)\[\e[0m\]\[\e[91m\]\[\e[0;91m\]\u@\h\[\e[0m\] in \[\e[0;38;5;33m\]\$PWD \[\e[0m\]$ \[\e[0m\]"

EOF
)

bashrc_tmpfile=$(mktemp -p .)
echo "$BASHRC" > $bashrc_tmpfile

docker build -t docker-kali -f - . << EOF
FROM kalilinux/kali-rolling

ENV DEBIAN_FRONTEND="noninteractive"
SHELL ["/bin/bash", "-c"]

ENV TOOLS_DIR="/opt/tools"
ENV GOROOT="/usr/local/go"
ENV GOPATH="/root/go"
ENV PATH="\$PATH:\$GOROOT/bin"
ENV PATH="\$PATH:\$GOPATH/bin"

RUN apt update -yqq && apt upgrade -yqq
RUN apt install --no-install-recommends -yqq $packages

COPY --from=golang:alpine /usr/local/go/ /usr/local/go/
RUN GO111MODULE=on go install -v github.com/projectdiscovery/httpx/cmd/httpx@latest
RUN GO111MODULE=on go install -v github.com/projectdiscovery/subfinder/v2/cmd/subfinder@latest
RUN GO111MODULE=on go install -v github.com/projectdiscovery/nuclei/v2/cmd/nuclei@latest
RUN GO111MODULE=on go install -v github.com/lc/gau/v2/cmd/gau@latest
RUN GO111MODULE=on go install -v github.com/OWASP/Amass/v3/...@master
RUN GO111MODULE=auto go get -v github.com/michenriksen/aquatone

$(
echo "RUN for dir in "${!tools[@]}"; do mkdir -p /opt/tools/\$dir; done"
)

$(
for i in "${!tools[@]}"; do
    echo "RUN for repo in "${tools[$i]}"; do git -C /opt/tools/$i clone \$repo; done"
    echo
done
)

$([[ ! -z $gems ]] && echo "RUN gem install $gems")

RUN tree --dirsfirst -n -L 2 \$TOOLS_DIR > /root/tools.txt

RUN apt clean -yqq && apt autoclean -yqq && apt autoremove --purge -yqq
RUN rm -rf /root/{.local,.cache,.wget-hsts}

COPY $bashrc_tmpfile /root/.bashrc

VOLUME /root/shared
WORKDIR /root
EOF

rm $bashrc_tmpfile

cat > $HOME/.local/bin/kali << "EOF"
#!/bin/bash

xhost +local:$(hostname) >/dev/null 2>&1
RAND=$(printf $(tr -dc 'a-z0-9' </dev/urandom | head -c 10))

case $1 in
    commit)
        docker run -it --name="docker-kali-commit" --hostname="docker-kali-commit" --net="host" docker-kali /bin/bash

        CID=$(docker container ls -a | grep -i docker-kali-commit | awk '{{print $1}}')
        docker commit $CID docker-kali
        docker container rm $CID --force
        ;;
    export)
        docker save docker-kali | xz -v6 > docker-kali.tar.xz
        ;;

#    vpn)
#        OVPN_CONFIG_PATH=$2
#        OVPN_CONFIG_NAME="$(echo $(basename $OVPN_CONFIG_PATH) | sed 's/@/-at-/')"
#        OVPN_CONFIG_FILE="$(basename $OVPN_CONFIG_PATH)"
#        OVPN_CONFIG_DIR="$(dirname $OVPN_CONFIG_PATH)"
#
#        if [[ -f $OVPN_CONFIG_PATH ]]; then
#            docker run --rm -it -d --cap-add=NET_ADMIN -v=$OVPN_CONFIG_DIR:/ovpn_configs:ro --dns=1.1.1.1 --dns=8.8.4.4 --dns=8.8.8.8 --device=/dev/net/tun --name=ovpn-client_$OVPN_CONFIG_NAME ovpn-client $OVPN_CONFIG_FILE >/dev/null
#
#        until [[ 'tun0' =~ $(docker exec -it "ovpn-client_${{OVPN_CONFIG_NAME}}" ip route get 8.8.8.8 | grep -Po '(?<=(dev )).*(?= src| proto)') ]]; do
#            echo "Waiting for VPN connection..."
#            sleep 1
#        done
#
#            # bug with --hostname="docker-kali_$OVPN_CONFIG_NAME" and --net=container
#            docker run --rm -it --name="docker-kali_$OVPN_CONFIG_NAME" --net=container:"ovpn-client_${{OVPN_CONFIG_NAME}}" -e DISPLAY=$DISPLAY -v=/tmp/.X11-unix:/tmp/.X11-unix -v=$HOME/.Xauthority:/root/.Xauthority:rw -v="$(pwd):/root/shared" docker-kali-custom /bin/bash && docker stop "ovpn-client_${{OVPN_CONFIG_NAME}}"
#        fi
#        ;;

        *)
            docker run --rm -it --name="docker-kali_$RAND" --hostname="docker-kali" --net="host" --privileged -e DISPLAY=$DISPLAY -v=/tmp/.X11-unix:/tmp/.X11-unix -v=$HOME/.Xauthority:/root/.Xauthority:rw -v="$(pwd):/root/shared" docker-kali /bin/bash
            ;;
esac

xhost -local:$(hostname) >/dev/null 2>&1
EOF
chmod +x $HOME/.local/bin/kali
