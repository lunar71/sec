#!/bin/bash

[[ -z $1 ]] && printf "Usage: $0 default | core | custom\n" && exit 1

[[ ! $(command -v docker) ]] && printf "Docker not found, install it first\n" && exit 2

# Extra packages/git repositories
wordlists="
    https://github.com/danielmiessler/SecLists
    https://github.com/FlameOfIgnis/Pwdb-Public
    https://github.com/swisskyrepo/PayloadsAllTheThings
    https://github.com/fuzzdb-project/fuzzdb
    https://github.com/1ndianl33t/Gf-Patterns
    https://github.com/dwisiswant0/gf-secrets
    https://github.com/danielmiessler/RobotsDisallowed
    https://github.com/cujanovic/subdomain-bruteforce-list
    https://github.com/cujanovic/Open-Redirect-Payloads
    https://github.com/cujanovic/Markdown-XSS-Payloads
    https://github.com/cujanovic/CRLF-Injection-Payloads
    https://github.com/cujanovic/Virtual-host-wordlist
    https://github.com/cujanovic/resolvers
    https://github.com/cujanovic/Linux-default-files-images-location
    https://github.com/arnaudsoullie/ics-default-passwords
    https://github.com/jeanphorn/wordlist
    https://github.com/nyxxxie/awesome-default-passwords
    https://github.com/1N3/IntruderPayloads
    https://github.com/berzerk0/Probable-Wordlists
    https://github.com/BBhacKing/jwt_secrets
    https://github.com/wallarm/jwt-secrets
    https://github.com/ihebski/DefaultCreds-cheat-sheet
    https://gist.github.com/8e12adefbd505ef704674ad6ad48743d.git
    https://github.com/random-robbie/bruteforce-lists
    https://github.com/six2dez/OneListForAll
" 

web="
    https://github.com/tomnomnom/waybackurls
    https://github.com/tomnomnom/httprobe
    https://github.com/tomnomnom/assetfinder
    https://github.com/lc/subjs
    https://github.com/lc/gau
    https://github.com/projectdiscovery/cloudlist
    https://github.com/projectdiscovery/naabu
    https://github.com/projectdiscovery/subfinder
    https://github.com/projectdiscovery/httpx
    https://github.com/projectdiscovery/nuclei
    https://github.com/OJ/gobuster
    https://github.com/michenriksen/aquatone
    https://github.com/ffuf/ffuf
    https://github.com/OWASP/Amass
    https://github.com/s0md3v/Arjun
    https://github.com/GerbenJavado/LinkFinder
    https://github.com/m4ll0k/SecretFinder
    https://github.com/brendan-rius/c-jwt-cracker
    https://github.com/OWASP/wstg
    https://github.com/maurosoria/dirsearch
    https://github.com/sa7mon/S3Scanner
    https://github.com/Dionach/CMSmap
    https://github.com/urbanadventurer/WhatWeb
    https://github.com/wpscanteam/wpscan
    https://github.com/zseano/InputScanner
    https://github.com/zseano/JS-Scan
    https://github.com/maK-/parameth
    https://github.com/drego85/JoomlaScan
    https://github.com/hakluke/hakrawler
    https://github.com/mbechler/marshalsec
    https://github.com/rbsec/sslscan
    https://github.com/IAmStoxe/urlgrab
    https://github.com/wireghoul/htshells
    https://github.com/m4ll0k/Bug-Bounty-Toolz
    https://github.com/chris408/ct-exposer
    https://github.com/mazen160/server-status_PWN
    https://github.com/s0md3v/Parth
    https://github.com/irsdl/IIS-ShortName-Scanner
"

mobile="
    https://github.com/AloneMonkey/frida-ios-dump
"

windows="
    https://github.com/thecybermafia/OffensivePowerShell
    https://github.com/mattifestation/PowerShellArsenal
    https://github.com/p3nt4/PowerShdll
    https://github.com/giMini/PowerMemory
    https://github.com/nickrod518/Create-EXEFromPS1
    https://github.com/leoloobeek/LAPSToolkit
    https://github.com/hfiref0x/UACME
    https://github.com/EncodeGroup/UAC-SilentClean
    https://github.com/padovah4ck/PSByPassCLM
    https://github.com/TheWover/donut
    https://github.com/fireeye/SharPersist
    https://github.com/Flangvik/SharpCollection
    https://github.com/Arvanaghi/SessionGopher
    https://github.com/giuliano108/SeBackupPrivilege
    https://github.com/NetSPI/PowerUpSQL
    https://github.com/PowerShellMafia/PowerSploit
    https://github.com/nidem/kerberoast
    https://github.com/Hackplayers/evil-winrm
    https://github.com/besimorhino/powercat
    https://github.com/p3nt4/Invoke-SocksProxy
    https://github.com/cyberark/RiskySPN
    https://github.com/GhostPack/Rubeus
    https://github.com/Kevin-Robertson/Inveigh
    https://github.com/Kevin-Robertson/Invoke-TheHash
    https://github.com/HarmJ0y/KeeThief
    https://github.com/HarmJ0y/SharpClipboard
    https://github.com/0x09AL/RdpThief
    https://github.com/rasta-mouse/AmsiScanBufferBypass
    https://github.com/outflanknl/Recon-AD
    https://github.com/0xthirteen/SharpRDP
    https://github.com/cobbr/SharpSploit
    https://github.com/NullArray/MaliciousDLLGen
    https://github.com/matterpreter/DefenderCheck
    https://github.com/Binject/go-donut
    https://github.com/samratashok/nishang
    https://github.com/GhostPack/Lockless
    https://github.com/GhostPack/SharpUp
    https://github.com/GhostPack/SafetyKatz
    https://github.com/GhostPack/SharpDump
    https://github.com/GhostPack/Seatbelt
    https://github.com/G0ldenGunSec/SharpSecDump
    https://github.com/Apr4h/CobaltStrikeScan
    https://github.com/mindcrypt/powerglot
    https://github.com/jfmaes/SharpXOR
    https://github.com/mez-0/DecryptRDCManager
    https://github.com/b4rtik/SharpKatz
    https://github.com/tokyoneon/chimera
    https://github.com/b4rtik/HiddenPowerShellDll
    https://github.com/Mr-Un1k0d3r/DLLsForHackers
    https://github.com/nccgroup/nccfsas
    https://github.com/trustedsec/unicorn
    https://github.com/411Hall/JAWS
    https://github.com/M4ximuss/Powerless
    https://github.com/rasta-mouse/Sherlock
    https://github.com/S3cur3Th1sSh1t/Amsi-Bypass-Powershell
    https://github.com/S3cur3Th1sSh1t/WinPwn
    https://github.com/CompassSecurity/BloodHoundQueries
    https://github.com/bohops/GhostBuild
    https://github.com/FortyNorthSecurity/hot-manchego
    https://github.com/mdsecactivebreach/SharpShooter
    https://github.com/passthehashbrowns/SharpBuster
    https://github.com/Greenwolf/ntlm_theft
    https://github.com/gtworek/PSBits
    https://github.com/OmerYa/Invisi-Shell
    https://github.com/mez-0/CSharpWinRM
    https://github.com/Gerenios/AADInternals
    https://github.com/samratashok/ADModule
    https://github.com/l0ss/Grouper2
    https://github.com/ropnop/kerbrute
    https://github.com/eladshamir/Internal-Monologue
    https://github.com/Kevin-Robertson/Powermad
    https://github.com/leechristensen/SpoolSample
    https://github.com/HarmJ0y/DAMP
    https://github.com/jschicht/RunAsTI
    https://github.com/Raikia/UhOh365
    https://github.com/nullbind/Powershellery
    https://github.com/PyroTek3/PowerShell-AD-Recon
    https://github.com/massgravel/Microsoft-Activation-Scripts
    https://github.com/Hackplayers/Salsa-tools
    https://github.com/dtrizna/DotNetInject
    https://github.com/rasta-mouse/TikiTorch
    https://github.com/yck1509/ConfuserEx
    https://github.com/peewpw/Invoke-PSImage
    https://github.com/Pickfordmatt/SharpLocker
    https://github.com/fullmetalcache/PowerLine
    https://github.com/Kevin-Robertson/InveighZero
    https://github.com/antonioCoco/RoguePotato
    https://github.com/ohpe/juicy-potato
    https://github.com/breenmachine/RottenPotatoNG
"

linux="
    https://github.com/Bashfuscator/Bashfuscator
    https://github.com/rebootuser/LinEnum
    https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite
    https://github.com/Anon-Exploiter/SUID3NUM
    https://github.com/sleventyeleven/linuxprivchecker
    https://github.com/diego-treitos/linux-smart-enumeration
    https://github.com/DominicBreuker/pspy
    https://github.com/jfredrickson/setuid-wrapper
    https://github.com/pentestmonkey/unix-privesc-check
    https://github.com/s0md3v/Decodify
    https://github.com/skelsec/pypykatz
    https://github.com/WangYihang/GitHacker
    https://github.com/BishopFox/GitGot
    https://github.com/TarlogicSecurity/tickey
    https://github.com/Zer1t0/ticket_converter
    https://github.com/TarlogicSecurity/kerbrute
    https://github.com/blendin/3snake
"

network="
    https://github.com/kost/revsocks
    https://github.com/jpillora/chisel
    https://github.com/bettercap/bettercap
    https://github.com/klsecservices/rpivot
    https://github.com/byt3bl33d3r/CrackMapExec
    https://github.com/robertdavidgraham/masscan
    https://github.com/BC-SECURITY/Empire
    https://github.com/BloodHoundAD/BloodHound
    https://github.com/fox-it/BloodHound.py
    https://github.com/threat9/routersploit
    https://github.com/ollypwn/SMBGhost
    https://github.com/lgandx/Responder
    https://github.com/nccgroup/vlan-hopping---frogger
    https://github.com/fox-it/mitm6
    https://github.com/ShawnDEvans/smbmap
    https://github.com/DanMcInerney/net-creds
    https://github.com/cytopia/pwncat
    https://github.com/rvrsh3ll/eavesarp
    https://github.com/lesnuages/hershell
    https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite
    https://github.com/RUB-NDS/PRET
    https://github.com/SecureAuthCorp/impacket
"

misc="
    https://github.com/paranoidninja/CarbonCopy
    https://github.com/projectdiscovery/notify
    https://github.com/andrew-d/static-binaries
    https://github.com/ZephrFish/static-tools
    https://github.com/static-linux/static-binaries-i386
    https://github.com/therealsaumil/static-arm-bins
    https://github.com/projectdiscovery/nuclei-templates
    https://github.com/vulnersCom/nmap-vulners
    https://github.com/pr4jwal/CVE-2020-0796
    https://github.com/HarmJ0y/CheatSheets
    https://github.com/ZerBea/hcxtools
    https://github.com/ZerBea/hcxdumptool
    https://github.com/hashcat/hashcat-utils
    https://github.com/sdcampbell/Internal-Pentest-Playbook
    https://github.com/kmkz/Pentesting
    https://github.com/megadose/holehe
    https://github.com/nil0x42/duplicut
    https://github.com/Wenzel/checksec.py
    https://github.com/jakejarvis/awesome-shodan-queries
    https://github.com/shifa123/shodandorks
    https://github.com/codebox/homoglyph
    https://github.com/tokyoneon/Arcane
    https://github.com/blackberry/pe_tree
    https://github.com/Ciphey/Ciphey
    https://github.com/vysecurity/LinkedInt
    https://github.com/awsmhacks/awsmBloodhoundCustomQueries
    https://github.com/S1ckB0y1337/Active-Directory-Exploitation-Cheat-Sheet
    https://github.com/leostat/rtfm
    https://github.com/dockcross/dockcross
    https://github.com/internetwache/GitTools
"

wifi="
    https://github.com/s0lst1c3/eaphammer
    https://github.com/wifiphisher/wifiphisher
"

bin="
    https://jitpack.io/com/github/frohoff/ysoserial/master-SNAPSHOT/ysoserial-master-SNAPSHOT.jar
    https://github.com/BloodHoundAD/BloodHound/releases/download/4.0.1/BloodHound-linux-x64.zip
    https://github.com/pwntester/ysoserial.net/releases/download/v1.34/ysoserial-1.34.zip 
    https://github.com/hashcat/hashcat/releases/download/v6.1.1/hashcat-6.1.1.7z
    https://github.com/gentilkiwi/mimikatz/releases/download/2.2.0-20200918-fix/mimikatz_trunk.7z
    https://raw.githubusercontent.com/iBotPeaches/Apktool/master/scripts/linux/apktool
    https://bitbucket.org/iBotPeaches/apktool/downloads/apktool_2.4.1.jar
    https://github.com/pxb1988/dex2jar/releases/download/2.0/dex-tools-2.0.zip
    https://github.com/appium-boneyard/sign/releases/download/1.0/sign-1.0.jar
    https://github.com/appium-boneyard/sign/releases/download/1.0/signapk-1.0.jar
    https://github.com/java-decompiler/jd-gui/releases/download/v1.6.6/jd-gui-1.6.6-min.jar
"

function default() {
    docker build -t docker-kali-default - << "EOF"
        FROM kalilinux/kali-rolling 
        ENV DEBIAN_FRONTEND=noninteractive
        RUN apt update -y \
            && apt dist-upgrade -y \
            && apt install -y kali-linux-default \
            && apt install -y musl \
            && apt autoremove -y \
            && apt clean -y

        COPY --from=golang:1.16-alpine /usr/local/go/ /usr/local/go/
        ENV GOROOT="/usr/local/go"
        ENV GOPATH="/root/go"
        ENV PATH="${PATH}:${GOROOT}/bin"
        ENV PATH="${PATH}:${GOPATH}/bin"

        RUN GO111MODULE=on go get -v github.com/projectdiscovery/httpx/cmd/httpx
        RUN GO111MODULE=on go get -v github.com/projectdiscovery/subfinder/v2/cmd/subfinder
        RUN GO111MODULE=on go get -v github.com/projectdiscovery/nuclei/v2/cmd/nuclei
        RUN GO111MODULE=on go get -v github.com/lc/gau
        RUN GO111MODULE=on go get -v github.com/OWASP/Amass/v3/...
        RUN GO111MODULE=auto go get -v github.com/michenriksen/aquatone

        RUN apt install -y mlocate && updatedb

        VOLUME /root/shared
        WORKDIR /root

        RUN printf '%s\n' "PS1='\[\e[0m\][\[\e[0;90m\]\d \[\e[0;90m\]\D{} \[\e[0;90m\]UTC\[\e[0m\]] \[\e[0;91m\]\h \[\e[0m\]in \[\e[0;38;5;33m\]\$PWD \[\e[0m\]\$ \[\e[0m\]'" >> /root/.bashrc
        RUN echo '[[ "$SCRIPT" != "RUNNING" ]] && SHELL=/bin/bash SCRIPT="RUNNING" script ~/shared/$(hostname)_$(date -u "+%d-%b-%Y_%H%M%S-UTC").log' >> /root/.bashrc
EOF
}

function core() {
    docker build -t docker-kali-core - << "EOF"
        FROM kalilinux/kali-rolling 
        ENV DEBIAN_FRONTEND=noninteractive
        RUN apt update -y \
            && apt dist-upgrade -y \
            && apt install -y kali-linux-core \
            && apt autoremove -y \
            && apt clean -y

        COPY --from=golang:1.16-alpine /usr/local/go/ /usr/local/go/
        ENV GOROOT="/usr/local/go"
        ENV GOPATH="/root/go"
        ENV PATH="${PATH}:${GOROOT}/bin"
        ENV PATH="${PATH}:${GOPATH}/bin"

        VOLUME /root/shared
        WORKDIR /root

        RUN printf '%s\n' "PS1='\[\e[0m\][\[\e[0;90m\]\d \[\e[0;90m\]\D{} \[\e[0;90m\]UTC\[\e[0m\]] \[\e[0;91m\]\h \[\e[0m\]in \[\e[0;38;5;33m\]\$PWD \[\e[0m\]\$ \[\e[0m\]'" >> /root/.bashrc
        RUN echo '[[ "$SCRIPT" != "RUNNING" ]] && SHELL=/bin/bash SCRIPT="RUNNING" script ~/shared/$(hostname)_$(date -u "+%d-%b-%Y_%H%M%S-UTC").log' >> /root/.bashrc
EOF
}

function custom() {
    docker build -t docker-kali-custom \
        --build-arg wordlists="${wordlists}" \
        --build-arg web="${web}" \
        --build-arg mobile="${mobile}" \
        --build-arg windows="${windows}" \
        --build-arg linux="${linux}" \
        --build-arg network="${network}" \
        --build-arg misc="${misc}" \
        --build-arg wifi="${wifi}" \
        --build-arg bin="${bin}" \
        - << "EOF"
        FROM kalilinux/kali-rolling 
        ENV DEBIAN_FRONTEND=noninteractive
        RUN apt update -y \
            && apt dist-upgrade -y \
            && apt install -y kali-linux-core \
            && apt install -y vim amass arp-scan arping \
                binwalk bettercap cadaver cewl chisel crackmapexec \
                ffuf dirsearch enum4linux exploitdb fping gobuster \
                hashcat hashcat-utils hydra impacket-scripts \
                maskprocessor masscan metasploit-framework nbtscan \
                netdiscover nikto nmap patator powersploit python3-pip \
                proxychains proxytunnel radare2 responder smbmap snmpcheck \
                sqlmap sslscan unix-privesc-check virtualenv voiphopper \
                wafw00f webshells weevely whatweb wordlists wpscan git \
                socat sqlitebrowser \
            && apt install -y musl tree \
            && apt autoremove -y \
            && apt clean -y

        COPY --from=golang:1.16-alpine /usr/local/go/ /usr/local/go/
        ENV GOROOT="/usr/local/go"
        ENV GOPATH="/root/go"
        ENV PATH="${PATH}:${GOROOT}/bin"
        ENV PATH="${PATH}:${GOPATH}/bin"

        RUN GO111MODULE=on go get -v github.com/projectdiscovery/httpx/cmd/httpx
        RUN GO111MODULE=on go get -v github.com/projectdiscovery/subfinder/v2/cmd/subfinder
        RUN GO111MODULE=on go get -v github.com/projectdiscovery/nuclei/v2/cmd/nuclei
        RUN GO111MODULE=on go get -v github.com/lc/gau
        RUN GO111MODULE=auto go get -v github.com/michenriksen/aquatone

        RUN pip3 install truffleHog

        RUN mkdir -p /root/shared
        VOLUME /root/shared
        WORKDIR /root

        RUN printf '%s\n' "PS1='\[\e[0m\][\[\e[0;90m\]\d \[\e[0;90m\]\D{} \[\e[0;90m\]UTC\[\e[0m\]] \[\e[0;91m\]\h \[\e[0m\]in \[\e[0;38;5;33m\]\$PWD \[\e[0m\]\$ \[\e[0m\]'" >> /root/.bashrc
        RUN echo '[[ "$SCRIPT" != "RUNNING" ]] && SHELL=/bin/bash SCRIPT="RUNNING" script ~/shared/$(hostname)_$(date -u "+%d-%b-%Y_%H%M%S-UTC").log' >> /root/.bashrc

        RUN apt install -y mlocate && updatedb
        RUN ["/bin/bash", "-c", "mkdir -p /opt/tools"]
        RUN printf "#!/bin/bash\n\nfind /opt/tools -mindepth 2 -maxdepth 2 -type d -print -exec git -C {} pull --quiet \;\n" > /opt/tools/update.sh
        RUN chmod +x /opt/tools/update.sh

        ARG wordlists
        RUN mkdir -p /opt/tools/wordlists
        RUN ["/bin/bash", "-c", "for i in $wordlists; do git -C /opt/tools/wordlists clone $i; done"]
        
        ARG web
        RUN mkdir -p /opt/tools/web
        RUN ["/bin/bash", "-c", "for i in $web; do git -C /opt/tools/web clone $i; done"]

        ARG mobile
        RUN mkdir -p /opt/tools/mobile
        RUN ["/bin/bash", "-c", "for i in $mobile; do git -C /opt/tools/mobile clone $i; done"]

        ARG windows 
        RUN mkdir -p /opt/tools/windows
        RUN ["/bin/bash", "-c", "for i in $windows; do git -C /opt/tools/windows clone $i; done"]

        ARG linux 
        RUN mkdir -p /opt/tools/linux
        RUN ["/bin/bash", "-c", "for i in $linux ; do git -C /opt/tools/linux clone $i; done"]

        ARG network 
        RUN mkdir -p /opt/tools/network
        RUN ["/bin/bash", "-c", "for i in $network; do git -C /opt/tools/network clone $i; done"]

        ARG misc 
        RUN mkdir -p /opt/tools/misc
        RUN ["/bin/bash", "-c", "for i in $misc; do git -C /opt/tools/misc clone $i; done"]

        ARG wifi
        RUN mkdir -p /opt/tools/wifi
        RUN ["/bin/bash", "-c", "for i in $wifi; do git -C /opt/tools/wifi clone $i; done"]

        ARG bin
        RUN mkdir -p /opt/tools/bin
        RUN ["/bin/bash", "-c", "for i in $bin; do wget -q --show-progress --no-check-certificate -nc $i -P /opt/tools/bin; done"]
        RUN ["/bin/bash", "-c", "wget -q --show-progress -nc https://download.sysinternals.com/files/SysinternalsSuite.zip -P /opt/tools/bin"]

        RUN tree --dirsfirst -n -L 2 /opt/tools/ > /root/tools.txt
EOF
}

function launcher() {
    TYPE=$1
    cat > $HOME/.local/bin/dkali <<-EOF
#!/bin/bash

xhost +local:\$(hostname) >/dev/null 2>&1
RAND=\$(printf \$(tr -dc 'a-z0-9' </dev/urandom | head -c 10))

case \$1 in
    commit)
        docker run -it --name="docker-kali-commit" --hostname="docker-kali-commit" --net="host" docker-kali-$TYPE /bin/bash

        CID=\$(docker container ls -a | grep -i docker-kali-commit | awk '{print \$1}')
        docker commit \$CID docker-kali
        docker container rm \$CID --force
        ;;
    *)
        docker run --rm -it --name="docker-kali_\$RAND" --hostname="docker-kali" --net="host" --privileged -e DISPLAY=\$DISPLAY -v=/tmp/.X11-unix:/tmp/.X11-unix -v=\$HOME/.Xauthority:/root/.Xauthority:rw -v="\$(pwd):/root/shared" docker-kali-$TYPE /bin/bash
        ;;
esac

xhost -local:\$(hostname) >/dev/null 2>&1
EOF
    chmod +x $HOME/.local/bin/dkali
}

case $1 in
    default)
        default
        launcher default;;
    core)
        core
        launcher core;;
    custom)
        custom
        launcher custom;;
esac

