#!/bin/bash

function usage() {
    cat << EOF
Usage: $0 <domain> <ip address>

e.g. $0 <example.com> <12.34.56.78>

Configure DNS management settings with two Glue Records pointing to ns1.<domain.com> and ns2.<domain.com>,
then use ns1.<domain.com> and ns2.<domain.com> as Custom Nameservers.

NOTE: if no DNS server is running, <domain.com> will not be resolved at all.
EOF
    exit 1
}

[[ $# -lt 2 ]] && usage

DOMAIN=$1
IPADDR=$2

IPADDR_REVERSED=$(printf ${IPADDR} | awk -F. '{print $4"."$3"."$2"."$1}')
LAST_OCTET=$(printf ${IPADDR} | awk -F. '{print $NF}')

BIND_CONFIG_DIR="bind"
BIND_ZONES_DIR="${BIND_CONFIG_DIR}/zones"

mkdir -p $BIND_ZONES_DIR

cat > "${BIND_CONFIG_DIR}/named.conf" << EOF
include "/etc/bind/named.conf.options";
include "/etc/bind/named.conf.local";
include "/etc/bind/named.conf.default-zones";


logging {
    channel query_log {
        file "/var/log/named/query.log" versions 3 size 5m;
        print-category yes;
        print-severity yes;
        print-time yes;
        severity dynamic;
        };
    channel update_debug {
        file "/var/log/named/update_debug.log" versions 3 size 5m;
        severity debug;
        print-category yes;
        print-severity yes;
        print-time yes;
        };
    channel security_info {
        file "/var/log/named/security_info.log" versions 3 size 5m;
        severity info;
        print-category yes;
        print-severity yes;
        print-time yes;
        };
    channel bind_log {
        file "/var/log/named/bind.log" versions 3 size 5m;
        severity info;
        print-category yes;
        print-severity yes;
        print-time yes;
        };
    category queries { query_log; };
    category security { security_info; };
    category update-security { update_debug; };
    category update { update_debug; };
    category lame-servers { null; };
    category default { bind_log; };
};
EOF

cat > "${BIND_CONFIG_DIR}/named.conf.local" << EOF
zone "${IPADDR_REVERSED}.in-addr.arpa" {
	type master;
	file "/etc/bind/zones/db.${LAST_OCTET}";
};

zone "${DOMAIN}" {
	type master;
	file "/etc/bind/zones/db.${DOMAIN}";
};
EOF

cat > "${BIND_CONFIG_DIR}/named.conf.options" << EOF
acl "trusted" {
	127.0.0.1;
};

options {
	directory "/var/cache/bind";
	dnssec-validation auto;
	auth-nxdomain no;
	listen-on-v6 { any; };
	allow-query { any; };
	recursion yes;
	allow-recursion { trusted; };
	allow-transfer { none; };
	forwarders {
		8.8.8.8;
		8.8.4.4;
	};
};
EOF

cat > "${BIND_ZONES_DIR}/db.${LAST_OCTET}" << EOF
\$TTL	604800
@	IN	SOA	${DOMAIN} root.localhost. (
			      5		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL
;
@       IN      NS      ns1.
10      IN      PTR     ns1.${DOMAIN}.
EOF

cat > "${BIND_ZONES_DIR}/db.${DOMAIN}" << EOF
\$TTL	604800
@	IN	SOA	${DOMAIN} root.localhost (
			      7		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL
;
@		IN	NS	ns1.${DOMAIN}.
@		IN	A	${IPADDR}
ns1		IN	A	${IPADDR}
*		IN	A	${IPADDR}
EOF

cat > "${BIND_CONFIG_DIR}/named.conf.default-zones" << EOF
zone "." {
	type hint;
	file "/usr/share/dns-root-hints/named.root";
};

zone "localhost" {
	type master;
	file "/etc/bind/db.local";
};

zone "127.in-addr.arpa" {
	type master;
	file "/etc/bind/db.127";
};

zone "0.in-addr.arpa" {
	type master;
	file "/etc/bind/db.0";
};

zone "255.in-addr.arpa" {
	type master;
	file "/etc/bind/db.255";
};
EOF

cat > "${BIND_CONFIG_DIR}/db.0" << EOF
\$TTL	604800
@	IN	SOA	localhost. root.localhost. (
			      1		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL
;
@	IN	NS	localhost.
EOF

cat > "${BIND_CONFIG_DIR}/db.127" << EOF
\$TTL	604800
@	IN	SOA	localhost. root.localhost. (
			      1		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL
;
@	IN	NS	localhost.
1.0.0	IN	PTR	localhost.
EOF

cat > "${BIND_CONFIG_DIR}/db.255" << EOF
\$TTL	604800
@	IN	SOA	localhost. root.localhost. (
			      1		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL
;
@	IN	NS	localhost.
EOF

cat > "${BIND_CONFIG_DIR}/db.local" << EOF
\$TTL	604800
@	IN	SOA	localhost. root.localhost. (
			      2		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL
;
@	IN	NS	localhost.
@	IN	A	127.0.0.1
@	IN	AAAA	::1
EOF

docker build -t docker-bind -f - . << EOF
    FROM alpine:latest
    RUN apk add --no-cache bind dns-root-hints
    RUN mkdir -p /var/log/named /var/cache/bind
    RUN chown root:named /var/log/named && chown root:named /etc/bind/*.conf*
    COPY bind /etc/bind
    CMD ["/usr/sbin/named", "-f", "-g"]
EOF

cat > $HOME/docker-bind-start.sh << EOF
#!/bin/bash

DATE_NOW=\$(date "+%d-%b-%Y_%H%M")

docker run --detach \
    --rm \
    --name=docker-bind \
    --hostname docker-bind \
    --publish=53:53/tcp \
    --publish=53:53/udp \
    docker-bind

nohup docker logs --follow docker-bind > "\${HOME}/docker-bind_\${DATE_NOW}.log" 2>&1 &
EOF

cat > $HOME/docker-bind-stop.sh << EOF
#!/bin/bash
docker container stop docker-bind
EOF

chmod +x $HOME/docker-bind-start.sh
chmod +x $HOME/docker-bind-stop.sh
rm -rf "${BIND_CONFIG_DIR}"
