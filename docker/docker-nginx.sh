#!/bin/bash

CONFIG=$(mktemp -p .)
cat > $CONFIG << EOF
user                 nginx;
pid                  /run/nginx.pid;
worker_processes     auto;
worker_rlimit_nofile 65535;

include              /etc/nginx/modules-enabled/*.conf;

events {
    multi_accept       on;
    worker_connections 65535;
}

http {
    charset                utf-8;
    sendfile               on;
    tcp_nopush             on;
    tcp_nodelay            on;
    server_tokens          off;
    log_not_found          on;
    types_hash_max_size    2048;
    types_hash_bucket_size 64;
    client_max_body_size   16M;

    include                mime.types;
    default_type           application/octet-stream;

    access_log             /var/log/nginx/access.log;
    error_log              /var/log/nginx/error.log warn;

    ssl_session_timeout    1d;
    ssl_session_cache      shared:SSL:10m;
    ssl_session_tickets    off;

    ssl_dhparam            /etc/nginx/dhparam.pem;

    ssl_protocols          SSLv3 TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
    ssl_ciphers            ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;

    ssl_stapling           off;
    ssl_stapling_verify    off;

    #include                /etc/nginx/conf.d/*.conf;

    server {
        listen                             80;
        listen                             443 ssl;
        server_name                        localhost;

        ssl_certificate                    /etc/nginx/ssl/server.crt;
        ssl_certificate_key                /etc/nginx/ssl/server.key;

        location / {
            root                        /var/www/html;

            # autoindex                 on;
            fancyindex                  on;
            fancyindex_exact_size       off;
            fancyindex_localtime        off;

            dav_methods                 PUT DELETE MKCOL COPY MOVE;
            dav_ext_methods             PROPFIND OPTIONS;
            dav_access                  user:rw group:rw all:rw;
            create_full_put_path        on;
            client_max_body_size        5000M;
            client_body_buffer_size     8k;
            client_body_temp_path       /tmp 1 2;

        }

        # gzip
        gzip            on;
        gzip_vary       on;
        gzip_proxied    any;
        gzip_comp_level 6;
        gzip_types      text/plain text/css text/xml application/json application/javascript application/rss+xml application/atom+xml image/svg+xml;
    }
}
EOF

ENTRYPOINT=$(mktemp -p .)
cat > $ENTRYPOINT << EOF
#!/bin/sh
usermod -u \$UID nginx
groupmod -g \$UID nginx
openssl req \
    -x509 \
    -newkey rsa:2048 \
    -days 3650 \
    -keyout /etc/nginx/ssl/server.key \
    -out /etc/nginx/ssl/server.crt \
    -nodes \
    -subj "/C=US/ST=State/L=Cityopia/O=Company/CN=127.0.0.1" >/dev/null 2>&1
nginx -g 'daemon off;'
EOF

docker build -t nginx-docker -f - . << EOF
FROM nginx:stable
RUN apt-get update -y && apt-get install -y openssl nginx-extras libnginx-mod-http-dav-ext apache2-utils pwgen
RUN rm -rf /var/www/html /etc/nginx/sites-enabled /etc/nginx/sites-available
RUN mkdir -p /etc/nginx/ssl /var/www/html
RUN openssl dhparam -out /etc/nginx/dhparam.pem 1024
RUN openssl req -x509 -newkey rsa:2048 -days 3650 -keyout /etc/nginx/ssl/server.key -out /etc/nginx/ssl/server.crt -nodes -subj "/C=US/ST=State/L=Cityopia/O=Company/CN=127.0.0.1"
COPY "$CONFIG" /etc/nginx/nginx.conf
COPY "$ENTRYPOINT" /entrypoint.sh
ENTRYPOINT ["/bin/sh", "/entrypoint.sh"]
EOF

rm $CONFIG $ENTRYPOINT

cat > "${HOME}/.local/bin/nginx-docker" << EOF
#!/bin/bash

[[ -z \$1 ]] && printf "Usage: \$0 <start/stop/logs>\n" && exit 1

case \$1 in
    "start")
        docker run -d -e "UID=\$UID" --rm --hostname="nginx-docker" --name="nginx-docker" -p80:80 -p443:443 --volume=\$(pwd):/var/www/html nginx-docker;;
    "stop")
        docker stop nginx-docker;;
    "logs")
        docker logs -f nginx-docker;;
esac
EOF
chmod +x "${HOME}/.local/bin/nginx-docker"
