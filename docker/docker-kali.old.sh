#!/usr/bin/env bash

mkdir -p $HOME/var/lib/docker
MYHOME=$HOME
cat << EOF | sudo tee /etc/docker/daemon.json
{
    "data-root": "$MYHOME/var/lib/docker"
}
EOF

sudo systemctl stop docker
sudo systemctl stop containerd

sudo systemctl start docker
sudo systemctl start containerd

docker run --name="kali-linux" \
    --hostname="kali-docker" \
    --net="host" \
    --privileged \
    -it \
    kalilinux/kali-rolling \
    /bin/bash -c "apt update -y && apt dist-upgrade -y && apt autoremove -y && apt clean -y && apt install kali-linux-default -y && apt autoremove -y && apt clean -y && exit"

CID=$(docker ps -a | grep -i kali-linux | awk '{print $1}') # container id
docker commit $CID kali-docker

cat >> $HOME/.bashrc << EOF

function dkali() {
    set -o posix
    local yn

    trap ctrl_c INT SIGINT
    function ctrl_c() {
        printf "\nTrapped ^C. Exiting...\n"
        yn="n"
    }

    docker run --name="dkali" --hostname="kali-docker" --net="host" --privileged -it --volume="\$(pwd):/root/mnt" kali-docker /bin/bash

    while true; do
        read -p "Do you want to commit changes to kali image? [Yy/Nn]: " yn
        CID=\$(docker container ls -a | grep -i dkali | awk '{print \$1}')
        case \$yn in
            [Yy]* ) docker commit \$CID kali-docker && docker container rm \$CID --force >/dev/null && \\
                        printf "Commited container changes to image\n" && break;;

            [Nn]* ) docker container rm \$CID --force >/dev/null && \\
                        printf "Discarded container\n" && set +o posix && break;;
        esac
    done
    trap - INT SIGINT
}
EOF
