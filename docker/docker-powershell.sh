#!/bin/bash

docker build -t docker-powershell - << "EOF"
    FROM mcr.microsoft.com/powershell:latest
    RUN apt-get update -y && apt-get upgrade -y
    RUN pwsh -Command Install-Module -Name PSWSMan -Force
    RUN pwsh -Command Install-WSMan
    RUN mkdir -p "/root/.config/powershell"
    RUN echo 'Function prompt { Write-Host "PS " -ForegroundColor Red -NoNewLine; Write-Host "in " -ForegroundColor White -NoNewLine; Write-Host "$(Get-Location) " -ForegroundColor Green -NoNewLine; Write-Host ">" -ForegroundColor White -NoNewLine; return " " }' > $HOME/.config/powershell/Microsoft.PowerShell_profile.ps1
EOF

cat > "${HOME}/.local/bin/dps" << EOF
    printf "Tips:\n"
    printf "Use \"Enter-PSSession -Credential (Get-Credential) -ComputerName <ip/hostname>\"\n\n"

    docker run --rm -it --name="docker-powershell" --hostname="docker-powershell" --volume="$(pwd):/root/shared" docker-powershell pwsh
EOF
chmod +x "${HOME}/.local/bin/dps"
